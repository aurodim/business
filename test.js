const redeemed = [];
let profit = 0;
let loss = 0;
let revenue = 0;

for (let redeem of redeemed) {
  revenue += redeem;
  loss += redeem > 5 ? 0.1 * redeem : 0;
  profit += redeem > 5 ? 0.9 * redeem : redeem;
}

console.log("\nRevenue:", revenue);
console.log("Loss:", loss);
console.log("---------------------");
console.log("Profit:", profit);
console.log("\n\n\n");


console.log("Revenue Based Off Tiers");
console.log("Lowest Tier:", 4 * profit);
console.log("Medium Tier:", 3 * profit);
console.log("Highest Tier:", 2.672 * profit);
console.log("\n\n\n");
console.log("Profit Based Off Tiers");
console.log("Lowest Tier:", (4 * profit) - loss);
console.log("Medium Tier:", (3 * profit) - loss);
console.log("Highest Tier:", (2.672 * profit) - loss);
