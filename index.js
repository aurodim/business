/*jshint esversion: 6 */
/*

  Mercial Business Definitions and Data Analysis

*/

var stats = require('simple-statistics'); // Library for Statistics https://simplestatistics.org/docs/

let m; // amount of merchips in video
let r; // reach [1,3]
let s; // seconds in video [0, ∞)
let c; // cost of video -> revenue in $USD
let o; // offer profit in terms of USD
let v; // # of views
let l; // # of payout to franchise
let e; // # of earnings from views
let g; // Gross Margin in USD
let oM; // offer price in terms of merchips
let oR; // # of times offer will be redeemed
let vR; // # of views to redeem once;
let ΣOE; // Operating Expenses / Overheads
let oI; // Operating Income / Profit Before Tax (PBT) ------------------- Gross Margin - ΣOE
let nI; // Net Income / Profit After Tax (PAT) -------------------------- PBT * Tax(in decimal);

let rawData = [
  {
    "name": "Josue Viramontes ",
    "views": 11,
    "secondsViewed": 406.288484,
    "merchips": 636
  },
  {
    "name": "Daisaq Cyrilla",
    "views": 24,
    "secondsViewed": 1344.9148330000003,
    "merchips": 2509
  },
  {
    "name": "Amelia",
    "views": 5,
    "secondsViewed": 279.45108300000004,
    "merchips": 567
  },
  {
    "name": "Ariel Isaias Rodriguez ",
    "views": 187,
    "secondsViewed": 11372.150366000013,
    "merchips": 30298
  },
  {
    "name": "Joshua Carmona ",
    "views": 1507,
    "secondsViewed": 147321.29443999872,
    "merchips": 443188
  },
  {
    "name": "Nihal Kodavarti ",
    "views": 15,
    "secondsViewed": 1321.976109,
    "merchips": 3918
  },
  {
    "name": "Saniya Dayal Kodavarti ",
    "views": 58,
    "secondsViewed": 2943.280539000002,
    "merchips": 8627
  },
  {
    "name": "Isaac Medina",
    "views": 17,
    "secondsViewed": 528.021434,
    "merchips": 1012
  },
  {
    "name": "Keyla Ramos",
    "views": 33,
    "secondsViewed": 1961.651932,
    "merchips": 4540
  },
  {
    "name": "Eliezer Flores",
    "views": 1029,
    "secondsViewed": 100352.80947399786,
    "merchips": 300255
  },
  {
    "name": "Daniel Castillo ",
    "views": 4,
    "secondsViewed": 322.460607,
    "merchips": 940
  },
  {
    "name": "Kevin Bonilla",
    "views": 27,
    "secondsViewed": 2514.3251459999997,
    "merchips": 7006
  },
  {
    "name": "Jezreel Rodriguez ",
    "views": 88,
    "secondsViewed": 5858.300981000001,
    "merchips": 14487
  },
  {
    "name": "Sam Vongsady ",
    "views": 33,
    "secondsViewed": 2737.3926599999986,
    "merchips": 7970
  },
  {
    "name": "Marco Alavez",
    "views": 31,
    "secondsViewed": 2308.973244,
    "merchips": 5228
  },
  {
    "name": "Rettany Umana ",
    "views": 96,
    "secondsViewed": 8323.945125000013,
    "merchips": 24225
  },
  {
    "name": "Ivonne  Jimenez  ",
    "views": 43,
    "secondsViewed": 2602.63288,
    "merchips": 5201
  },
  {
    "name": "Eric castillo",
    "views": 59,
    "secondsViewed": 5246.350389000002,
    "merchips": 12704
  },
  {
    "name": "Arnie Santoyo",
    "views": 52,
    "secondsViewed": 4111.054924999999,
    "merchips": 12016
  },
  {
    "name": "Hector Abrego ",
    "views": 77,
    "secondsViewed": 3825.2257659999977,
    "merchips": 8546
  }
];
let secondsViewedArr = [];
let merchipsArr = [];
let viewsArr = [];

rawData.forEach(data => {
  secondsViewedArr.push(data.secondsViewed);
  merchipsArr.push(data.merchips);
  viewsArr.push(data.views);
});

let secondsViewedMean = stats.mean(secondsViewedArr);
let merchipsMean = stats.mean(merchipsArr);
let viewsMean = stats.mean(viewsArr);

// Ideally the exchange ration from Merchip to USD is 1 : 0.001

// The Prices for the videos should be:

    // 0.002
    // 0.003
    // 0.004

// A Video of x Seconds long with Reach y

r = 3;
s = 1;
m = r * s;

c = s * (0.001 + 0.001*r);

// An offer with price x;
console.log("Cost", c);
o = 2;
oM = Math.ceil((o/0.2) / 0.006);

console.log("# of Merchips needed to redeem:", oM);

// Sample user has x views

v = 100000000 * 30 * 365;
oR = oM == 0 ? 0 : Math.floor((m * v) / oM);
l = o * oR;
e = v * c;
g = e - l;
vR = Math.ceil(oM / m);



// Print out all of our info
console.log("Seconds:", s);
console.log("Seconds Spent Watching for one redeem:", (s * vR).toFixed(2));
console.log("Minutes Spent Watching for one redeem:", (s * vR / 60).toFixed(2));

console.log("\n");

console.log("Views: ", v);
console.log("Offer Payout:", o);
console.log("# of Views for one redeem:", vR);
console.log("# of Redeems:", oR);

console.log("\n");

console.log("Revenue:", e);
console.log("Loss:", l);
console.log("Gross Margin:", g);

console.log("\n");

// Other Statistics



// For a more complete report we can analyze the expenses that come in the year and use the averages we have gathered to predict theexpected values

/*

  The Following is a breakdown of the expenses with detailed comments on each

  This is what is financially known as FIXED COSTS / OPERATING EXPENSES / OVERHEADS

  $89 per month for the Office that is used as the companies main address
      Helps with image and is legally required -> 233 S Wacker Dr 84 FL AKA Willis Tower
      $89 * 12 -> A total of $1068 for the office per year

  $20 per month for GSuite the admin panel for recieveing and sending emails through the Google service.
      $20 * 12 -> A total of $140 per year

  $39 per month for Vyond, a video editing software for our animated explainer videos
      $39 * 12 -> A total of $468 per year

  $99	per month for Cloudinary, a storage cloud for videos
      $99 * 12 -> A total of $1188 per year

  $12 per year for GoDaddy, host for website

  $360 per month for MLab, MongoDB hosting website
      $360 * 12 -> A  total of $4320

  $1100 per year for Taxes, U.S

  $200 per year for Annual Report, U.S



  Σ(Expenses) = $8496

*/

ΣOE = 8496;
console.log("Operating Expenses:",ΣOE);


// We have our Operating Income which is equal to the Gross Margin minus Operating Expenses

oI = g - ΣOE;
console.log("Operating Income:",oI);

// Next, we have our Net Income which is the Operating Income minus the other expenses such as Taxes
nI = oI > 0 ? oI * 0.8 : oI; // (20% of income Tax)
console.log("Net Income:",nI.toFixed(2));


// Based on the current amount of shares:
// Daniel : 43%
// Isaac : 43%
// Yoana : 4%
// Eliu : 10%

console.log("\n");

console.log("Daniel:", (nI * 0.34).toFixed(2));
console.log("Isaac:", (nI * 0.34).toFixed(2));
console.log("Eliu:", (nI * 0.28).toFixed(2));
console.log("Yoana:", (nI * 0.04).toFixed(2));

console.log("\n");

console.log("\n\n\n");
console.log("================ Data Analysis ===================\n");
console.log("Span:","~1.5 weeks --- 11 days");
console.log("Subjects:", rawData.length);
console.log("\n");
console.log("Merchips Mean:",merchipsMean);
console.log("Seconds Viewed Mean:",secondsViewedMean);
console.log("Views Mean:", viewsMean);
console.log("\n");
console.log("Average Merchips per day:", (merchipsMean/11).toFixed(2));
console.log("Average seconds per day:", (secondsViewedMean/11/20).toFixed(2));
console.log("Average views per day:", (viewsMean/11).toFixed(2));
